from odoo import fields, models, api
import datetime
from datetime import timedelta
import logging

class Partner(models.Model):
    _inherit = 'res.partner'
    _description = 'oeh medical patient'
 #   _inherit = ['mail.thread','mail.activity.mixin']


    MARITAL_STATUS = [
        ('Single', 'Single'),
        ('Married', 'Married'),
        ('Widowed', 'Widowed'),
        ('Divorced', 'Divorced'),
        ('Separated', 'Separated'),
    ]

    SEX = [
        ('Male', 'Male'),
        ('Female', 'Female'),
    ]


    BLOOD_TYPE = [
        ('A', 'A'),
        ('B', 'B'),
        ('AB', 'AB'),
        ('O', 'O'),
    ]

    RH = [
        ('+','+'),
        ('-','-'),
    ]

    def _patient_age(self):
        def compute_age_from_dates(patient_dob, patient_deceased, patient_dod):
            now = datetime.datetime.now()
            if (patient_dob):
                dob = datetime.datetime.strptime(patient_dob.strftime('%Y-%m-%d'), '%Y-%m-%d')
                if patient_deceased:
                    dod = datetime.datetime.strptime(patient_dod.strftime('%Y-%m-%d'), '%Y-%m-%d')
                    delta = dod - dob
                    deceased = " (deceased)"
                    years_months_days = str(delta.days // 365) + " years " + str(delta.days % 365) + " days" + deceased
                else:
                    delta = now - dob
                    years_months_days = str(delta.days // 365) + " years " + str(delta.days % 365) + " days"
            else:
                years_months_days = "No DoB !"

            return years_months_days

        for patient_data in self:
            patient_data.age = compute_age_from_dates(patient_data.dob, patient_data.deceased, patient_data.dod)
        return True

 #   edad = fields.Char("edad", default=False)
    dob = fields.Date(string='Date of Birth')
    age = fields.Char(compute=_patient_age, size=32, string='Patient Age', help="It shows the age of the patient in years(y), months(m) and days(d).\nIf the patient has died, the age shown is the age at time of death, the age corresponding to the date on the death certificate. It will show also \"deceased\" on the field")

    notes = fields.Text(string="Note")

    dadname = fields.Char(string='Nombre del Padre')
    momname = fields.Char(string='Nombre de la Madre')
    deceased = fields.Boolean(string='Patient Deceased ?', help="Mark if the patient has died")
    dod = fields.Date(string='Date of Death')
    marital_status = fields.Selection(MARITAL_STATUS, string='Marital Status')
    sex = fields.Selection(SEX, string='Sex', index=True)
    blood_type = fields.Selection(BLOOD_TYPE, string='Blood Type')
    rh = fields.Selection(RH, string='Rh')



    fieltype = fields.Selection(string='Type',selection=[('val1', 'Val1'), ('val2', 'Val2')])
    field1 = fields.Char(string='Field1')
    field2 = fields.Char(string='Field2')


    space = fields.Char(' ', readonly=True)