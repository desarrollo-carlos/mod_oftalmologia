# -*- coding: utf-8 -*-
{
    'name': "mod_oftalmologia",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        #'security/oeh_security.xml',
        'sequence/oeh_sequence.xml',
        'oeh_navigation.xml',
        #'oeh_oftalmologia/views/patient_view.xml',
        'oeh_oftalmologia/views/doctor_view.xml',
        'oeh_oftalmologia/views/partner.xml',

       # 'oeh_oftalmologia_prueba/views/oeh_medical_ophthalmology_view.xml',
    
        #'oeh_walkins/views/oeh_medical_register_for_walkin_view.xml',
        'data/oeh_physician_degrees.xml',
        'oeh_patient_call_log/views/oeh_medical_patient_call_log_view.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}