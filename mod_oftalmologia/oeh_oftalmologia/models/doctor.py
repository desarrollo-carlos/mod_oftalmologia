# -*- coding: utf-8 -*-

from odoo import fields, models, api
import datetime
from datetime import timedelta


class doctorRegister(models.Model):
    _name = 'mod.doctor.oftal'
    _description = 'oeh medical doctor'
 #   _inherit = ['mail.thread','mail.activity.mixin']

    CONSULTATION_TYPE = [
        ('Residential', 'Residential'),
        ('Visiting', 'Visiting'),
        ('Other', 'Other'),
    ]


    APPOINTMENT_TYPE = [
        ('Not on Weekly Schedule', 'Not on Weekly Schedule'),
        ('On Weekly Schedule', 'On Weekly Schedule'),
    ]


    name = fields.Many2one('res.partner', string='Nombre',required=True,)
    code = fields.Char(string='Code', size=128, help="ie, ADP")

    speciality = fields.Many2one(string='Speciality', help="Speciality Code")


   # degree_id = fields.Many2many(id1='physician_id', id2='degree_id', string='Degrees')

    institution = fields.Many2one(string='Institution', help="Institution where doctor works")
    space = fields.Char(' ', readonly=True)


    consultancy_price = fields.Integer(string='Consultancy Charge', help="Physician's Consultancy price")
    appointment_type = fields.Selection(APPOINTMENT_TYPE, string='Allow Appointment on?', default=lambda *a: 'Not on Weekly Schedule')
   # available_lines = fields.One2many('oeh.medical.physician.line', 'physician_id', string='Physician Availability')


    consultancy_type = fields.Selection(CONSULTATION_TYPE, string='Consultancy Type', help="Type of Doctor's Consultancy", default=lambda *a: 'Residential')

    is_pharmacist = fields.Boolean(string='Pharmacist?', default=lambda *a: False)


    is_pharmacist = fields.Boolean(string='Paciente', default=lambda *a: False)
    is_doctor = fields.Boolean(string='Doctor', default=lambda *a: False)

    oeh_user_id = fields.Many2one('res.users', string='Responsible Odoo User')



#degree_id = fields.Many2many('oeh.medical.degrees', id1='physician_id', id2='degree_id', string='Degrees')
#institution = fields.Many2one('oeh.medical.health.center', string='Health Center', help="Medical Center", readonly=True, states={'Scheduled': [('readonly', False)]})
#consultancy_type = fields.Selection(CONSULTATION_TYPE, string='Consultancy Type', help="Type of Doctor's Consultancy", default=lambda *a: 'Residential')
#consultancy_price = fields.Integer(string='Consultancy Charge', help="Physician's Consultancy price")
#institution = fields.Many2one('oeh.medical.health.center', string='Health Center', help="Medical Center", readonly=True, states={'Scheduled': [('readonly', False)]})
