from odoo import fields, models, api
import datetime
from datetime import timedelta
import logging


class Paciente(models.Model):
    _name = 'mod.paciente'
    _description = 'datos de interfaz paciente'
    _inherits={
        'res.partner': 'patient',
    }

    MARITAL_STATUS = [
        ('Single', 'Single'),
        ('Married', 'Married'),
        ('Widowed', 'Widowed'),
        ('Divorced', 'Divorced'),
        ('Separated', 'Separated'),
    ]

    SEX = [
        ('Male', 'Male'),
        ('Female', 'Female'),
    ]


    BLOOD_TYPE = [
        ('A', 'A'),
        ('B', 'B'),
        ('AB', 'AB'),
        ('O', 'O'),
    ]

    RH = [
        ('+','+'),
        ('-','-'),
    ]

    def _patient_age(self):
        def compute_age_from_dates(patient_dob, patient_deceased, patient_dod):
            now = datetime.datetime.now()
            if (patient_dob):
                dob = datetime.datetime.strptime(patient_dob.strftime('%Y-%m-%d'), '%Y-%m-%d')
                if patient_deceased:
                    dod = datetime.datetime.strptime(patient_dod.strftime('%Y-%m-%d'), '%Y-%m-%d')
                    delta = dod - dob
                    deceased = " (deceased)"
                    years_months_days = str(delta.days // 365) + " years " + str(delta.days % 365) + " days" + deceased
                else:
                    delta = now - dob
                    years_months_days = str(delta.days // 365) + " years " + str(delta.days % 365) + " days"
            else:
                years_months_days = "No DoB !"

            return years_months_days

        for patient_data in self:
            patient_data.age = compute_age_from_dates(patient_data.dob, patient_data.deceased, patient_data.dod)
        return True



    def _vaccine_count(self):
        oe_vac = self.env['oeh.patient.call.log']
        for va in self:
            domain = [('patient', '=', va.id)]
            vec_ids = oe_vac.search(domain)
            vecs = oe_vac.browse(vec_ids)
            vecs_count = 0
            for vac in vecs:
                vecs_count+=1
            va.vaccine_count = vecs_count
        return True


    def _prescription_count(self):
        oe_pres = self.env['oeh.ophthalmology.prueba']
        for pa in self:
            domain = [('patient', '=', pa.id)]
            pres_ids = oe_pres.search(domain)
            pres = oe_pres.browse(pres_ids)
            pres_count = 0
            for pr in pres:
                pres_count+=1
            pa.prescription_count = pres_count
        return True
    
    def _walkin_count(self):
        oe_apps = self.env['oeh.medical.appointment']
        for pa in self:
            domain = [('patient', '=', pa.id)]
            app_ids = oe_apps.search(domain)
            apps = oe_apps.browse(app_ids)
            app_count = 0
            for ap in apps:
                app_count+=1
            pa.citas_count = app_count
        return True



 #   edad = fields.Char("edad", default=False)
    patient = fields.Many2one('res.partner', string='Nombre',required=True, ondelete='cascade', help='Partner-related data of the patient')
    dob = fields.Date(string='Date of Birth')
    age = fields.Char(compute=_patient_age, size=32, string='Patient Age', help="It shows the age of the patient in years(y), months(m) and days(d).\nIf the patient has died, the age shown is the age at time of death, the age corresponding to the date on the death certificate. It will show also \"deceased\" on the field")

    notes = fields.Text(string="Note")

    dadname = fields.Char(string='Nombre del Padre')
    momname = fields.Char(string='Nombre de la Madre')

    deceased = fields.Boolean(string='Patient Deceased ?', help="Mark if the patient has died")
    dod = fields.Date(string='Date of Death')
    marital_status = fields.Selection(MARITAL_STATUS, string='Marital Status')
    sex = fields.Selection(SEX, string='Sex', index=True)
    blood_type = fields.Selection(BLOOD_TYPE, string='Blood Type')
    rh = fields.Selection(RH, string='Rh')
    critical_info = fields.Text(string='Important disease, allergy or procedures information', help="Write any important information on the patient's disease, surgeries, allergies, ...")
    general_info = fields.Text(string='General Information', help="General information about the patient")
    blood_ty = fields.Selection(BLOOD_TYPE, string='Blood Type')
    fieltype = fields.Selection(string='Type',selection=[('val1', 'Val1'), ('val2', 'Val2')])
    field1 = fields.Char(string='Field1')
    field2 = fields.Char(string='Field2')

    identification_code = fields.Char(string='Patient ID', size=256, help='Patient Identifier provided by the Health Center', readonly=True)
    space = fields.Char(' ', readonly=True)



    vaccine_count = fields.Integer(compute=_vaccine_count, string="LLamadas")
    
    prescription_count = fields.Integer(compute=_prescription_count, string="LLamadas")

    citas_count = fields.Integer(compute=_walkin_count, string="Citas")


    is_pharmacist = fields.Boolean(string='Paciente', default=lambda *a: False)
    is_doctor = fields.Boolean(string='Doctor', default=lambda *a: False)
    is_patient = fields.Boolean(string='Patient', help='Check if the party is a patient')

    


    evaluation_ids = fields.One2many('oeh.ophthalmology.prueba', 'walkin', string='Evaluation', readonly=True, states={'Scheduled': [('readonly', False)]})


    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].next_by_code('oeh.medical.patient')
        vals['identification_code'] = sequence
        vals['is_patient'] = True
        health_patient = super(Paciente, self).create(vals)
        return health_patient
    

    @api.onchange('patient')
    def onchange_patient(self):
        if self.patient:
            self.dob = self.patient.dob
            self.sex = self.patient.sex
            self.marital_status = self.patient.marital_status
            self.blood_type = self.patient.blood_type
            self.rh = self.patient.rh


class OeHealthPrescriptions(models.Model):
    _name = 'oeh.ophthalmology.prueba'
    _description = 'Prescriptions'

    

class OeHealthPatientEvaluation(models.Model):
    _inherit = 'oeh.ophthalmology.prueba'
    _description = 'oeh.medical.oftalmologia'
    walkin = fields.Many2one('mod.paciente', string='Queue #')