from odoo import fields, models, api
import datetime
from datetime import timedelta
import logging






class Partner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'
    _description = 'oeh medical patient'
 #   _inherit = ['mail.thread','mail.activity.mixin']


    MARITAL_STATUS = [
        ('Single', 'Single'),
        ('Married', 'Married'),
        ('Widowed', 'Widowed'),
        ('Divorced', 'Divorced'),
        ('Separated', 'Separated'),
    ]

    SEX = [
        ('Male', 'Male'),
        ('Female', 'Female'),
    ]


    BLOOD_TYPE = [
        ('A', 'A'),
        ('B', 'B'),
        ('AB', 'AB'),
        ('O', 'O'),
    ]

    RH = [
        ('+','+'),
        ('-','-'),
    ]

    CONSULTATION_TYPE = [
        ('Residential', 'Residential'),
        ('Visiting', 'Visiting'),
        ('Other', 'Other'),
    ]

    def _patient_age(self):
        def compute_age_from_dates(patient_dob, patient_deceased, patient_dod):
            now = datetime.datetime.now()
            if (patient_dob):
                dob = datetime.datetime.strptime(patient_dob.strftime('%Y-%m-%d'), '%Y-%m-%d')
                if patient_deceased:
                    dod = datetime.datetime.strptime(patient_dod.strftime('%Y-%m-%d'), '%Y-%m-%d')
                    delta = dod - dob
                    deceased = " (deceased)"
                    years_months_days = str(delta.days // 365) + " years " + str(delta.days % 365) + " days" + deceased
                else:
                    delta = now - dob
                    years_months_days = str(delta.days // 365) + " years " + str(delta.days % 365) + " days"
            else:
                years_months_days = "No DoB !"

            return years_months_days

        for patient_data in self:
            patient_data.age = compute_age_from_dates(patient_data.dob, patient_data.deceased, patient_data.dod)
        return True

 #   edad = fields.Char("edad", default=False)
    dob = fields.Date(string='Date of Birth')
    age = fields.Char(compute=_patient_age, size=32, string='Patient Age', help="It shows the age of the patient in years(y), months(m) and days(d).\nIf the patient has died, the age shown is the age at time of death, the age corresponding to the date on the death certificate. It will show also \"deceased\" on the field")

    notes = fields.Text(string="Note")

    dadname = fields.Char(string='Nombre del Padre')
    momname = fields.Char(string='Nombre de la Madre')
    deceased = fields.Boolean(string='Patient Deceased ?', help="Mark if the patient has died")
    dod = fields.Date(string='Date of Death')
    marital_status = fields.Selection(MARITAL_STATUS, string='Marital Status')
    sex = fields.Selection(SEX, string='Sex', index=True)
    blood_type = fields.Selection(BLOOD_TYPE, string='Blood Type')
    rh = fields.Selection(RH, string='Rh')



    fieltype = fields.Selection(string='Type',selection=[('val1', 'Val1'), ('val2', 'Val2')])
    field1 = fields.Char(string='Field1')
    field2 = fields.Char(string='Field2')


    space = fields.Char(' ', readonly=True)


    dod = fields.Date(string='Date of Death')
    cod = fields.Many2one('oeh.medical.pathology', string='Cause of Death')


    is_pharmacist = fields.Boolean(string='Paciente', default=lambda *a: False)
    is_doctor = fields.Boolean(string='Doctor', default=lambda *a: False)
    is_patient = fields.Boolean(string='Patient', help='Check if the party is a patient')




    code = fields.Char(string='Licence ID', size=128, help="Physician's License ID")
    degree_id = fields.Many2many('oeh.medical.degrees1', id1='physician_id', id2='degree_id', string='Degrees')
    consultancy_type = fields.Selection(CONSULTATION_TYPE, string='Consultancy Type', help="Type of Doctor's Consultancy", default=lambda *a: 'Residential')
    consultancy_price = fields.Integer(string='Consultancy Charge', help="Physician's Consultancy price")

    


    receta = fields.Many2one('oeh.ophthalmology.prueba', string="Receta")

class OeHealthPhysicianDegree(models.Model):
    _name = "oeh.medical.degrees1"
    _description = "Physicians Degrees"

    name = fields.Char(string='Degree', size=128, required=True)
    full_name = fields.Char(string='Full Name', size=128)
    physician_ids = fields.Many2many('oeh.medical.physician', id1='degree_id', id2='physician_id', string='Physicians')

    _sql_constraints = [
        ('full_name_uniq', 'unique (name)', 'The Medical Degree must be unique')]



class OeHealthPhysician(models.Model):
    _name = "oeh.medical.physician"
    _description = "Information about the doctor"


    CONSULTATION_TYPE = [
        ('Residential', 'Residential'),
        ('Visiting', 'Visiting'),
        ('Other', 'Other'),
    ]

    APPOINTMENT_TYPE = [
        ('Not on Weekly Schedule', 'Not on Weekly Schedule'),
        ('On Weekly Schedule', 'On Weekly Schedule'),
    ]



